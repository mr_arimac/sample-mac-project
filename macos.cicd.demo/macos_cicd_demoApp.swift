//
//  macos_cicd_demoApp.swift
//  macos.cicd.demo
//
//  Created by Fahim Farook on 2021-05-26.
//

import SwiftUI

@main
struct macos_cicd_demoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
